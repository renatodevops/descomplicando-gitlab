#!/bin/bash

hostname gitlab-server
echo gitlab-server > /etc/hostname
apt install -y curl openssh-server ca-certificates tzdata perl
apt install -y postfix
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | sudo bash
apt update
apt install gitlab-ce

# cat /etc/gitlab/initial_root_password
# vi gitlab.rb
# timedatectl list-timezones
# gitlab-ctl reconfigure
# ps -ef | grep git
