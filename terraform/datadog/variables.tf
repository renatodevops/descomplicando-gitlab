variable "DD_API_KEY" {
  type = string
}

variable "DD_APP_KEY" {
  type = string
}

variable "api_url" {
  default = "https://us5.datadoghq.com/"
}

