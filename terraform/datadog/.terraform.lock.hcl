# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/datadog/datadog" {
  version = "3.24.0"
  hashes = [
    "h1:gl/UsD2rUS6m2qOIhb3FNnHyi0zr0ATYILaHNHW5vzE=",
    "zh:129a71497f54186fe8f5e8ea8f0da8a14b6222937c818a0decc5fdf48b546f2d",
    "zh:1650911334b2030e5e8d8749ee69f84c0321a6d0ed2cd25d91faac9116c55bb1",
    "zh:2beb4c7aed190908a8ed07560d7d242396035acad3de335fd1e374f6f8f807b6",
    "zh:2f957395de1e5c877cd0554fd829fd35215c1fbac90839f86b932e02547ca857",
    "zh:369c14de9ba1a00eb5f2705163bd2d1cb22df6dcbd3d46957593e737b8d04901",
    "zh:38d515bfaac1db6dae6e48e96208e815a89219839f001ad3226665deb6f2172a",
    "zh:48cf8ee45c01db53a299ce502f2052baf8caf9cdf2487210dda58161e572a5ba",
    "zh:5d2d8881934d266188dccc110415d044e42dabbde6bce3c7522216f515b244dd",
    "zh:8604e9d181bb2c5f9c5a4b4b7bbe391bedefdce9bc48578fb4dc022f456a384e",
    "zh:98215ec26950cae9e5154b48327372e876e196635aa9ca49375909818c5b88e0",
    "zh:c553cc4c24e3887026a30ce38f64c913a5172b9c04e7caf608118c733148f032",
    "zh:e4054edff368ccd3719531c2eda8312ce957a72f061e6a6ab928be1b6e732f4d",
    "zh:f507393653c0a5f67e4b6ff84f36f60871944a1a084c0772c4b28544bc526624",
    "zh:f568bd32248e725d34c4a0ed33f807035c6f962da45a9e2e6812780cf44baa9b",
  ]
}
